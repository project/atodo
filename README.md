# @Todo

Drush is a very nice framework for building CLI tools. But it's not a proper
ecosystem until it has a todo app.

@Todo is intended to provide developers and administrators with a lightweight
task-recording tool that can replace burying @todos in your code. Those can
sometimes be useful to inform others of future intent, but are a pain to
actually track your own actions. With new or tiny projects, I often find 
myself putting high-level notes in an issue tracker somewhere but jotting
down more detailed action items in a hard to manage scratch space... or a
postIt note. @Todo provides cleaner structure and is easily there for me
on the command-line when I want to drop a quick thought into the queue.

Again, it is not intended to compete with a real issue tracker. Feature
requests oriented at making @Todo scale to more users or more issues will
likely be rejected.

## Localized, Contextual Tasks
@Todo leverages it's own concept of project context to bundle together
tasks. In decreasing order of immediacy, it searches for:

 * A Git repository.
 * A Drupal core directory.
 * Your home directory.

What does that mean? Well, when you type

    `$> drush todo-add "menu_router performance patch"`

from any location within a Git repository, it will be grouped with every other
todo you created in that repo. This means your entire project's todos are
readily bundled, and viewable with a quick `drush todo-list`.

## A Feature for Several Occasions

 * Sort by date (oldest to newest)
 * Sort by priority (oldest has highest priority, except when the --top option
   is used to slide things in at the top of the heap.)
 * Relative dates (you don't always need to see the whole rigamarole.)
 * Updating existing todos with new text, or bumping to the top.
 * Searching through todos in a bundle with ID# or description (with regex support).

## A Few Good Aliases

This tool is so independent from typical Drush activities that you could fool
folks into not realizing they were drushin' about their tasks. A few aliases
save your fingers time. Drop these in the vicinity of a .bashrc file:

 * alias todo='drush todo-add -y'
 * alias todos='drush todo-list'
 * alias todox='drush todo-remove'
 * alias todomsg='drush todo-list --pipe --limit=1'

## Usage
And now, a couple quick examples:

    $> todo "Take out the trash."
    $> git commit -m `todomsg --search="menu_router"`

## The Things People Say They Want

So, NOT AN ISSUE TRACKER, but if you find you are using @Todo and have
practical use cases that would be met with a reasonable feature, that might
be arranged. A few things from the @Todo-list associated with my working
directory:

 * Add --all option to todo-remove to delete the entire list.
 * Search across all todo-lists.
 * Select a @Todo-list and synchronize it's items into the current context.

