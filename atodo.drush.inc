<?php
/**
 * @file
 * Code for the @Todo command.
 */

/**
 * Implementation of hook_drush_help().
 */
function atodo_drush_help($section) {
  switch ($section) {
    case 'meta:atodo:title':
      return dt('Drush Todo commands');
    case 'meta:pm:summary':
      return dt('Use and manage a simple, local todo list.');
    case 'drush:todo-update':
      return dt('Update an item in the todo list. If no change is specified item will move to the bottom of the list.');
    case 'error:ATODO_WRITE_FAILURE':
      return dt('Could not save @todo changes.');
    case 'error:ATODO_NO_ID':
      return dt('No @todo ID provided.');
    case 'error:ATODO_INVALID_ID':
      return dt('Invalid @todo ID supplied.');
  }
}

/**
 * Implements hook_drush_command().
 */
function atodo_drush_command() {
  $items = array();

  $items['todo-list'] = array(
    'description' => 'List the items in the todo list.',
    'options' => array(
      'search' => 'May specify the Todo ID # or a fragment of the todo text to filter the todo list.',
      'here' => 'Limit issues to those associated with current directory.',
      'by-date' => 'Sort the todos by date ascending instead of priority.',
      'limit' => 'Limit the number of todos presented to the viewer to the specified number.',
    ),
    'bootstrap' => 'DRUSH_BOOTSTRAP_DRUSH',
    'aliases' => array('todos'),
  );

  $items['todo-add'] = array(
    'description' => 'Add an item to the todo list.',
    'arguments' => array(
      'item' => 'Description of the task to be done. Recommend one-liners.',
    ),
    'options' => array(
      'top' => 'Insert new todos as the top, instead of at the bottom of the list.',
    ),
    'bootstrap' => 'DRUSH_BOOTSTRAP_DRUSH',
    'aliases' => array('todo'),
  );

  $items['todo-update'] = array(
    'description' => 'Update an item in the todo list.',
    'arguments' => array(
      'id' => 'ID of the todo you want to update.',
    ),
    'options' => array(
      'description' => 'Description of the todo.',
      'top' => 'Move the todo to the top of the list.',
    ),
    'bootstrap' => 'DRUSH_BOOTSTRAP_DRUSH',
    'aliases' => array('todo-up'),
  );

  $items['todo-remove'] = array(
    'description' => 'Remove an item from the todo list.',
    'arguments' => array(
      'id' => 'Specify the Todo ID # to delete. If one is not specified, you will be presented with optoins.',
    ),
    'options' => array(
      'search' => 'May specify the Todo ID # or a fragment of the todo text to filter the todo list.',
      'here' => 'Limit issues to those associated with current directory.',
    ),
    'bootstrap' => 'DRUSH_BOOTSTRAP_DRUSH',
    'aliases' => array('todo-rm', 'todox'),
  );

  $items['todo-path'] = array(
    'description' => 'Retrieve the active path at which a specified todo was created.',
    'arguments' => array(
      'id' => 'Specify the Todo ID # to look up.',
    ),
    'options' => array(
      'context' => 'Instead of a todo path, present the current todo context.',
      'storage' => 'Instead of a todo path, present the location of the current todo file.',
    ),
    'bootstrap' => 'DRUSH_BOOTSTRAP_DRUSH',
    'aliases' => array('todop'),
  );

  return $items;
}

/**
 * Command callback for 'todo-list'.
 */
function drush_atodo_todo_list() {
  $todos = atodo_get_todos();

  if (!empty($todos['entries'])) {
    atodo_sort_todos($todos['entries']);
    if ($limit = drush_get_option('limit', FALSE)) {
      $todos['entries'] = array_slice($todos['entries'], 0, $limit);
    }
    atodo_print_todos($todos);
  }
  else {
    drush_log('You have no todos. Use `drush todo-add {task}` to add one.', 'status');
  }
}

// Add a new @todo. //

/**
 * Implements drush_hook_COMMAND_validate() for todo-add.
 */
function drush_atodo_todo_add_validate($item = NULL) {
  if (!isset($item)) {
    drush_set_error('ATODO_NO_ITEM', dt('No @todo description provided.'));
  }
}

/**
 * Command callback for 'todo-add'.
 */
function drush_atodo_todo_add($item) {
  $proceed = drush_confirm(dt('Are you sure you want to add a new @todo?'));
  if (!$proceed) {
    return;
  }
  
  // Given an item description, produce the basic task data structure with
  // available data. Any change to the order should result in tweaking
  // atodo_print_todos(). As a nice @todo, consider investigating creating
  // drush_print_table_assoc().
  $todo = array(
    'description' => $item,
    'date' => time(),
    'path' => drush_cwd(),
  );

  if ($todo = atodo_write_item($todo)) {
    drush_log(dt('Added "!item" as @todo #!id. Type "drush todo-list --search=!id" to view it.', array(
      '!item' => $item,
      '!id' => $todo['id'],
    )), 'success');
  }
  
  if ($todo['id'] > 50) {
    drush_print('The @Todo team recommends getting a more sophisticated issue tracker about now.');
  }
}

// Remove an existing @todo. //

/**
 * Implements drush_hook_COMMAND_validate() for todo-remove.
 */
function drush_atodo_todo_remove_validate($id = NULL) {
  atodo_validate_id($id);
}

/**
 * Command callback for 'todo-remove'.
 */
function drush_atodo_todo_remove($id) {
  $proceed = drush_confirm(dt('Are you sure you want to delete @todo #!id?', array('!id' => $id)));
  
  if ($proceed && $removed = atodo_delete_item($id)) {
    drush_log(dt('Removed item "!item".', array(
      '!item' => '[' . $removed['id'] . '] ' . $removed['description'],
    )), 'success');
  }
}

// Update an existing @todo. //

/**
 * Implements drush_hook_COMMAND_validate() for todo-update.
 */
function drush_atodo_todo_update_validate($id = NULL) {
  atodo_validate_id($id);
}

/**
 * Command callback for 'todo-update'.
 */
function drush_atodo_todo_update($id) {
  $proceed = drush_confirm(dt('Are you sure you want to update @todo #!id?', array('!id' => $id)));

  if ($proceed && $removed = atodo_delete_item($id)) {
    drush_log(dt('Removed @item from todo list. Preparing replacement...', array('@item' => $id)));

    // If an item is specified it replaces the previous description.
    if ($item = drush_get_option('description', FALSE)) {
      $removed['description'] = $item;
    }

    // Rewrite the data. If no new description is supplied, this will bump its
    // weight to the bottom of the list (or with --top, to the top.)
    if (atodo_write_item($removed, TRUE)) {
      drush_log(dt('Updated item "!item".', array(
        '!item' => '[' . $removed['id'] . '] ' . $removed['description'],
      )), 'success');
    }
  }
}

// Retrieve path context info for a @todo. //

/**
 * Implements drush_hook_COMMAND_validate() for todo-path.
 */
function drush_atodo_todo_path_validate($id = NULL) {
  if (!drush_get_option('context', FALSE) && !drush_get_option('storage', FALSE)) {
    atodo_validate_id($id);
  }
}

/**
 * Command callback for 'todo-path'.
 */
function drush_atodo_todo_path($id = NULL) {
  if (drush_get_option('context', FALSE)) {
    $data = atodo_get_context();
  }
  elseif (drush_get_option('storage', FALSE)) {
    $data = atodo_filename();
  }
  else {
    $todos = atodo_get_todos();
    $data = $todos['entries'][$id][$path];
  }

  drush_print($data);
  drush_print_pipe($data);
}

// @Todo API //

/**
 * Evaluate the presence and validity of an item ID.
 *
 * @param int $id
 *   (Default: NULL) ID to check.
 *
 * @return boolean
 *   Will routine TRUE if a valid ID is supplied.
 */
function atodo_validate_id($id = NULL) {
  if (!isset($id)) {
    return drush_set_error('ATODO_NO_ID');
  }
  $todos = atodo_get_todos();
  if (!isset($todos['entries'][$id])) {
    return drush_set_error('ATODO_INVALID_ID');
  }

  return TRUE;
}

/**
 * Load contextually relevant todos into memory.
 *
 * @param boolean $reset
 *   Reload the todos.
 *
 * @return array
 */
function atodo_get_todos($reset = FALSE) {
  $todos = atodo_load_all_todos($reset);

  // Filter todo list by --search parameter, if present.
  if ($search = drush_get_option('search', FALSE)) {
    drush_log(dt('Todo listing limited by search term "@search".', array('@search' => $search)));
    if (is_numeric($search)) {
      if (isset($todos['entries'][$search])) {
        $todos['entries'] = array($search => $todos['entries'][$search]);
      }
      else {
        $todos['entries'] = array();
      }
    }
    elseif (is_string($search)) {
      $todos['entries'] = array_filter($todos['entries'], function ($todo) use ($search) {
        return preg_match('/' . $search . '/S', $todo['description']);
      });
    }

    if (empty($todos['entries'])) {
      drush_log(dt('No result was found for search "!search".', array(
        '!search' => $search,
      )), 'status');
    }
  }

  // Restrict todo list to those created in subdirectories of the current path.
  if (drush_get_option('here', FALSE)) {
    $todos = array_filter($todos['entries'], function ($todo) {
      return strpos($todo['path'], drush_cwd()) === 0;
    });
  }

  return $todos;
}

/**
 * Load todos into memory.
 *
 * @param boolean $reset
 *   Reload the todos.
 *
 * @return array
 */
function atodo_load_all_todos($reset = FALSE) {
  static $todos;

  if (!isset($todos) || $reset) {
    $todos = @file_get_contents(atodo_filename());
    if (empty($todos)) {
      return array();
    }
    $todos = drush_json_decode($todos);
  }

  return $todos;
}

/**
 * Get the todo "context".
 *
 * Priority is first a Git repo, second your home directory.
 */
function atodo_get_context() {
  static $context;

  if (!isset($context)) {
    $context = atodo_determine_git_context();

    // If the user is not working in a git repository (gasp!), check to see if
    // there is a valid Drupal root.
    if (empty($context)) {
      $context = drush_get_context('DRUSH_SELECTED_DRUPAL_ROOT', FALSE);
    }
    if (empty($context)) {
      $context = drush_server_home();
    }
    drush_log(dt('Setting todo context to !path.', array('!path' => $context)));
  }

  return $context;
}

/**
 * Determine the details of any relevant Git context.
 */
function atodo_determine_git_context() {
  // Use the most common stable remote.
  $git_context = drush_get_option('git-context', 'origin');
  drush_shell_exec("git config --get remote.{$git_context}.url");
  $results = drush_shell_exec_output();  
  $context = !empty($results[0]) ? $results[0] : FALSE;

  // Default to "local" Git context computation if the remote context did not exist.
  if (empty($context)) {
    // If you happen to have repository set via your drushrc.php file, skip the
    // redundant check. However, since not everyone uses that sort of trick, we
    // cannot assume its absence means we do not have a Git repo context.
    $context = drush_get_option('repository', FALSE);
    if (!$context) {
      if (drush_shell_exec('git rev-parse --show-toplevel')) {
        $results = drush_shell_exec_output();
        $context = $results[0];
      }
    }  
  }

  return $context;
}

// Rendering Items //

/**
 * Sort the todos if specified.
 */
function atodo_sort_todos(&$items) {
  if (drush_get_option('by-date', FALSE)) {
    usort($items, function($a, $b) {
      if ($a == $b) {
        return 0;
      }
      return $a['date'] < $b['date'] ? -1 : 1; 
    });
  }
}

/**
 * Render todo list to the screen.
 */
function atodo_print_todos($todos) {
  $items = $todos['entries'];
  if (empty($items)) {
    return;
  }

  if (drush_get_option('pipe', FALSE)) {
    $pipe = array();
    foreach ($items as $todo) {
      $pipe[] = $todo['description'];
    }
    drush_print_pipe($pipe);
    return;
  }

  // Remove unneeded metadata and reformat the rest as needed.
  foreach ($items as &$todo) {
    $todo['date'] = atodo_relative_date($todo['date']);
    unset($todo['path']);
  }

  // Add headers.
  $headers = array('id' => dt('ID'), 'description' => 'Description', 'date' => 'Created');
  array_unshift($items, $headers);

  $name = basename(atodo_get_context());
  drush_print(dt('!count Todos for !name', array('!count' => count($items) - 1, '!name' => $name)));
  drush_print_table($items, TRUE);
}

// CRUD //

/**
 * Add a new todo to the list.
 *
 * @param array $todo
 * @param boolean $reset
 *
 * @return boolean
 */
function atodo_write_item($todo, $reset = FALSE) {
  $todos = atodo_load_all_todos($reset);

  // Initialize a new todo list.
  if (empty($todos)) {
    $todos = array(
      'max_id' => 0,
      'created' => time(),
      'entries' => array(),
      'context' => atodo_get_context(),
      'generator' => '@Todo',
      'generator url' => 'http://drupal.org/project/atodo',
      'generator version' => '0.2',
    );
  }

  if (!isset($todo['id'])) {
    // Increment ID counter and add to todo.
    $todos['max_id']++;
    atodo_array_unshift_assoc($todo, 'id', $todos['max_id']);
  }
  if (drush_get_option('top', FALSE)) {
    atodo_array_unshift_assoc($todos['entries'], $todo['id'], $todo);
  }
  else {
    $todos['entries'][$todo['id']] = $todo;
  }
  $todos = json_encode($todos);

  if (drush_get_context('DRUSH_SIMULATE')) {
    return TRUE;
  }
  
  if (@file_put_contents(atodo_filename(), $todos)) {
    return $todo;
  }

  return drush_set_error('ATODO_WRITE_FAILURE');
}

/**
 * Remove a todo as identified by ID#.
 *
 * @param int $id
 *   Todo ID to delete.
 *
 * @return boolean
 */
function atodo_delete_item($id) {
  $todos = atodo_load_all_todos();

  $removed = $todos['entries'][$id];
  unset($todos['entries'][$id]);
  $todos = drush_json_encode($todos);
  if (drush_get_context('DRUSH_SIMULATE')) {
    return $removed;
  }
  elseif (@file_put_contents(atodo_filename(), $todos)) {
    return $removed;
  }

  return drush_set_error('ATODO_WRITE_FAILURE');
}

function atodo_delete_file() {
 // atodo_filename();
  
}

/**
 * Get the file path to the contextual todo file.
 */
function atodo_filename() {
  $path = drush_get_option('todo-path', drush_server_home() . '/.todo');
  if (drush_mkdir($path, TRUE)) {
    return $path .= '/todo_' . atodo_escape_path(atodo_get_context()) . '.json';
  }
}

// Helper functions. @todo: Push some of these upstream? //

/**
 * Insert an associative array element to the start of an array.
 *
 * @see http://www.php.net/manual/en/function.array-unshift.php#107448
 */
function atodo_array_unshift_assoc(&$arr, $key, $val) {
  $arr = array_reverse($arr, true);
  $arr[$key] = $val;
  $arr = array_reverse($arr, true);
  return $arr;
}

/**
 * OS-agnostic conversion of a directory path to a safe filename.
 *
 * @param string path
 *   Path to convert.
 *
 * @return string
 */
function atodo_escape_path($path) {
  $path = strtr($path, '/\:.@', '-----');
  return $path;
}

/**
 * Format a unixtime timestamp into a quick-read date.
 *
 * @see http://drupal.org/project/reldate.
 */
function atodo_relative_date($timestamp) {
  static $slots;

  $day = date('MjY', $timestamp);
  if (!isset($slots)) {
    $now = time();
    $slots = array(
      'today' => date('MjY', $now),
      'yesterday' => date('MjY', $now - 86400),
      'month' => $now - 2592000,
    );
  }

  if ($day == $slots['today']) {
    return date('g:ia', $timestamp) . ' Today';
  }
  elseif ($day == $slots['yesterday']) {
    return date('g:ia', $timestamp) . ' Yesterday';
  }
  elseif ($timestamp > $slots['month']) {
    return date('g:ia D M j', $timestamp);
  }
  else {
    return date('g:ia M j, Y', $timestamp);
  }
}
